package main

import (
	"fmt"

	"adventofcode2021/utils"
)

func partOne() int {
	input := utils.ReadInt()
	var result int

	for i := 1; i < len(input); i++ {
		if input[i] > input[i-1] {
			result++
		}
	}

	return result
}

func partTwo() int {

	input := utils.ReadInt()
	var result int
	var sums []int

	for i := 0; i < len(input)-2; i++ {
		add := input[i] + input[i+1] + input[i+2]
		sums = append(sums, add)
	}

	for i := 1; i < len(sums); i++ {
		if sums[i] > sums[i-1] {
			result++
		}
	}

	return result
}

func main() {
	p1 := partOne()
	fmt.Println("result for part one is :", p1)
	p2 := partTwo()
	fmt.Println("result for part two is :", p2)
}
