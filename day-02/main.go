package main

import (
	"adventofcode2021/utils"
	"fmt"
	"strconv"
	"strings"
)

func partOne() int {
	input := utils.ReadString()
	var result int

	type Position struct {
		h int
		d int
	}
	var travel Position

	for _, v := range input {
		sep := strings.Split(v, " ")
		value, _ := strconv.Atoi(sep[1])
		switch sep[0] {
		case "forward":
			travel.h = travel.h + value
		case "up":
			travel.d = travel.d - value
		case "down":
			travel.d = travel.d + value
		}
	}

	result = travel.h * travel.d
	return result
}

func partTwo() int {
	input := utils.ReadString()
	var result int

	type Position struct {
		h int
		d int
		a int
	}
	var travel Position

	for _, v := range input {
		sep := strings.Split(v, " ")
		value, _ := strconv.Atoi(sep[1])
		switch sep[0] {
		case "forward":
			travel.h = travel.h + value
			travel.d = travel.d + travel.a*value
		case "up":
			travel.a = travel.a - value
		case "down":
			travel.a = travel.a + value
		}
	}

	result = travel.h * travel.d
	return result
}

func main() {
	p1 := partOne()
	fmt.Println("result for part one is :", p1)
	p2 := partTwo()
	fmt.Println("result for part two is :", p2)
}
