package main

import (
	"adventofcode2021/utils"
	"fmt"
	"log"
	"strconv"
	"strings"
)

type grid struct {
	num  [5][5]int
	mark [5][5]bool
}

func partOne() int {
	input := utils.ReadString()
	var result int

	drawn := strings.Split(input[0], ",")
	var drawnInt []int
	for _, v := range drawn {
		vInt, _ := strconv.Atoi(v)
		drawnInt = append(drawnInt, vInt)
	}

	grids := make([]*grid, 0)

	for i := 2; i < len(input)-1; {
		newGrid := &grid{}
		for index := range newGrid.num {
			line := input[i+index]
			lineSlice := strings.Split(line, " ")
			var lineInt []int
			for _, v := range lineSlice {
				if v != "" {
					vInt, _ := strconv.Atoi(v)
					lineInt = append(lineInt, vInt)
				}
			}
			var lineArr [5]int
			copy(lineArr[:], lineInt)
			newGrid.num[index] = lineArr
		}
		grids = append(grids, newGrid)
		i = i + 6
	}

	bingo := winBoard(drawnInt, grids)

	log.Printf("bingo: %#+v\n", bingo)
	return result
}

func winBoard(drawn []int, grids []*grid) (board grid) {
	for _, num := range drawn {
		for _, grid := range grids {
			grid.punch(num)
		}
	}
	return
}

func (g *grid) punch(n int) bool {
	for l := 0; l < 5; l++ {
		for c := 0; c < 5; c++ {
			if g.num[l][c] == n {
				g.mark[l][c] = true
			}
		}
	}
	for i := 0; i < 5; i++ {
		for j := 0; j < 5; j++ {
			isFullLine, isFullCol := true, true
			log.Printf("g.marklin: %#+v\n", g.mark[i][j])
			log.Printf("g.markcol: %#+v\n", g.mark[j][i])
			if !g.mark[i][j] {
				isFullLine = false
			}
			if !g.mark[j][i] {
				isFullCol = false
			}
			if isFullLine || isFullCol {
				return true
			}
		}

	}
	return false
}

func partTwo() int {
	return 0
}

func main() {
	p1 := partOne()
	fmt.Println("result for part one is :", p1)
	p2 := partTwo()
	fmt.Println("result for part two is :", p2)
}
