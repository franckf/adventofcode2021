package main

import (
	"fmt"
	"log"
	"strconv"
	"strings"

	"adventofcode2021/utils"
)

type ventLine struct {
	x1 int
	y1 int
	x2 int
	y2 int
}

func partOne() int {
	input := utils.ReadString()
	var result int
	var inputVent []ventLine

	for _, line := range input {
		var newvent ventLine
		sep12 := strings.Split(line, "->")
		part1 := strings.Split(sep12[0], ",")
		part2 := strings.Split(sep12[1], ",")
		newvent.x1, _ = strconv.Atoi(strings.Trim(part1[0], " "))
		newvent.y1, _ = strconv.Atoi(strings.Trim(part1[1], " "))
		newvent.x2, _ = strconv.Atoi(strings.Trim(part2[0], " "))
		newvent.y2, _ = strconv.Atoi(strings.Trim(part2[1], " "))
		inputVent = append(inputVent, newvent)
	}
	log.Printf("inputVent: %#+v\n", inputVent)
	return result
}

func partTwo() int {
	var result int
	return result
}

func main() {
	p1 := partOne()
	fmt.Println("result for part one is :", p1)
	p2 := partTwo()
	fmt.Println("result for part two is :", p2)
}
