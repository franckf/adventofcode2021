package utils

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func openInput() *os.File {
	file, err := os.Open("input.txt")
	if err != nil {
		fmt.Println(err)
	}
	return file
}

func ReadString() []string {
	file := openInput()
	defer file.Close()
	var result = []string{}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		result = append(result, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		fmt.Println(err)
	}
	return result
}

func ReadInt() []int {
	file := openInput()
	defer file.Close()
	var result = []int{}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		convertedInt, err := strconv.Atoi(scanner.Text())
		if err != nil {
			fmt.Println(err)
		}
		result = append(result, convertedInt)
	}

	if err := scanner.Err(); err != nil {
		fmt.Println(err)
	}
	return result
}
