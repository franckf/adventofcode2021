package main

import (
	"adventofcode2021/utils"
	"fmt"
	"strconv"
	"strings"
)

func partOne() int {
	input := utils.ReadString()
	var result int
	var count0 [12]int
	var count1 [12]int

	for _, v := range input {
		for i := 0; i < 12; i++ {
			vbytes := v[i]
			switch vbytes {
			case 48: //0
				count0[i]++
			case 49: //1
				count1[i]++
			}
		}
	}

	var gamma [12]string
	var epsilon [12]string
	for i := 0; i < 12; i++ {
		if count0[i] > count1[i] {
			gamma[i] = "0"
			epsilon[i] = "1"
		} else {
			gamma[i] = "1"
			epsilon[i] = "0"
		}
	}

	gammaBin := strings.Join(gamma[:], "")
	epsilonBin := strings.Join(epsilon[:], "")
	gammaDec, _ := strconv.ParseInt(gammaBin, 2, 64)
	epsilonDec, _ := strconv.ParseInt(epsilonBin, 2, 64)

	result = int(gammaDec) * int(epsilonDec)
	return result
}

func partTwo() int {
	inputOxy := utils.ReadString()
	inputCo2 := utils.ReadString()
	var result int
	var oxy string
	var co2 string

	for i := 0; i < 12; i++ {
		count0 := 0
		count1 := 0
		var tmp []string

		for _, v := range inputOxy {
			vbytes := v[i]
			switch vbytes {
			case 48: //0
				count0++
			case 49: //1
				count1++
			}
		}

		for _, v := range inputOxy {
			if count1 >= count0 {
				if v[i] == 49 {
					tmp = append(tmp, v)
				}
			} else {
				if v[i] == 48 {
					tmp = append(tmp, v)
				}
			}
		}
		inputOxy = tmp
		if len(inputOxy) == 1 {
			oxy = inputOxy[0]
		}
	}

	for i := 0; i < 12; i++ {
		count0 := 0
		count1 := 0
		var tmp []string

		for _, v := range inputCo2 {
			vbytes := v[i]
			switch vbytes {
			case 48: //0
				count0++
			case 49: //1
				count1++
			}
		}

		for _, v := range inputCo2 {
			if count1 < count0 {
				if v[i] == 49 {
					tmp = append(tmp, v)
				}
			} else {
				if v[i] == 48 {
					tmp = append(tmp, v)
				}
			}
		}
		inputCo2 = tmp
		if len(inputCo2) == 1 {
			co2 = inputCo2[0]
		}
	}

	oxyDec, _ := strconv.ParseInt(oxy, 2, 64)
	co2Dec, _ := strconv.ParseInt(co2, 2, 64)
	result = int(oxyDec) * int(co2Dec)
	return result
}

func main() {
	p1 := partOne()
	fmt.Println("result for part one is :", p1)
	p2 := partTwo()
	fmt.Println("result for part two is :", p2)
}
